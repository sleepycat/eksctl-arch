# eksctl for Archlinux

This is simple Archlinux packaging for the `eksctl` command from Weaveworks.
For details see the [weaveworks/eksctl](https://github.com/weaveworks/eksctl) repo.
